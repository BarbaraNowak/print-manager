# PrintManager

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Po pobraniu projektu z repozytorium należy zainstalować moduły i zależności z pliku package.json. Poniższe polecenie należy uruchomić w konsoli z poziomu zerowego projektu.

npm install

## Uruchomienie projektu w oknie przeglądarki

ng serve -o



# Dodatkowa konfiguracja:

## Ustawienie lokalnej (customowej) ścieżki w pliku environment.js oraz environments.prod.js

const APP_URL = 'http://127.0.0.1/angular/mc-comp/print-manager';
