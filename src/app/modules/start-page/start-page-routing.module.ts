import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Module */
/* Containers */
import * as startContainers from './components';
import { StartPageModule } from './start-page.module';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: startContainers.StartPageComponent,
    },

];

@NgModule({
    imports: [StartPageModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class StartPageRoutingModule {}
