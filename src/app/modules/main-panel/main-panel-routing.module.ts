import { ErrorComponent } from './components/error/error.component';
import { MainPanelModule } from './main-panel.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Containers */
import * as mainPanelContainers from './components';


/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: mainPanelContainers.MainPanelComponent,
    },
    {
      path: 'create',
      pathMatch: 'full',
      component: mainPanelContainers.PrinterFormComponent,
    },
    {
      path: 'edit/:id',
      pathMatch: 'full',
      component: mainPanelContainers.PrinterFormComponent,
    },
    {
      path: 'view/:id',
      pathMatch: 'full',
      component: mainPanelContainers.PrinterViewComponent,
    },
    {
      path: '**',
      pathMatch: 'full',
      component: mainPanelContainers.ErrorComponent
    },
];

@NgModule({
    imports: [MainPanelModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class MainPanelRoutingModule {}
