import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import * as mainPanelContainers from './components';
import { MainPanelComponent } from './components/main-panel/main-panel.component';
import { PrintersListComponent } from './components/printers-list/printers-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ErrorComponent } from './components/error/error.component';

@NgModule({
    declarations: [
      mainPanelContainers.containers,
      MainPanelComponent,
      PrintersListComponent,
      ErrorComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule
    ],
    exports: [mainPanelContainers.containers],
})
export class MainPanelModule {}
