import { Component, OnInit, Input } from '@angular/core';
import { BreadcrumbData } from 'src/app/global-data/navigation/breadcrumb-data';
import * as $ from 'jquery';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  @Input() breadcrumbs: BreadcrumbData[];
  leftPanelStatus: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  changeDisplayLeftPanel(){
    if(this.leftPanelStatus){
      $(".layoutSideNav").animate({
        width:"0px"
      })
      $('.layoutSideNavContent').animate({
        paddingLeft:"0px"
      })
      this.leftPanelStatus = false;
    }else{
      $(".layoutSideNav").animate({
        width:"255px"
      })
      $('.layoutSideNavContent').animate({
        paddingLeft:"255px"
      })
      this.leftPanelStatus = true;
    }

  }

}
