import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
    loaderSize = [1, 2, 3, 4];
    @Input() loaderStatus: boolean;
    @Input() content: string;

    constructor() {}

    ngOnInit(): void {}
}
