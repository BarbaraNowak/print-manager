import { Component, OnInit, Input } from '@angular/core';
import { BreadcrumbData } from 'src/app/global-data/navigation/breadcrumb-data';

@Component({
  selector: 'app-layout-panel',
  templateUrl: './layout-panel.component.html',
  styleUrls: ['./layout-panel.component.scss']
})
export class LayoutPanelComponent implements OnInit {

  constructor() { }
  @Input() breadcrumbs: BreadcrumbData[];
  @Input() loaderStatus: boolean;
  @Input() content: string;

  ngOnInit(): void {
  }

}
