import { environment } from './../../../../../environments/environment.prod';
import { cloneDeep } from 'lodash';
import { DataManagerService } from './../../../../global-services/data-manager.service';
import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { Printer } from './../../../../global-data/printers';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Data } from 'src/app/global-data/data';
import { PRINTER_COLORS } from 'src/app/global-data/printer-colors';
import { PRINTER_STATUS } from 'src/app/global-data/printer-status';

@Component({
  selector: 'app-printers-list',
  templateUrl: './printers-list.component.html',
  styleUrls: ['./printers-list.component.scss'],
})
export class PrintersListComponent implements OnInit {
  dataStorage: Data | any = {
    printers:[]
  };
  closeForm = 0;

  searchText : string = '';
  copyPrinters: Printer[] = [];
  loaderStatus: boolean = true;
  loaderContent: string;
  printerColors = PRINTER_COLORS;

  modalOptions: NgbModalOptions;
  modalWindow = {
    type: '',
    title: '',
    message: '',
    status: false,
    activePrinter:null,
    rightButtonContent: '',
  };

  tablePrinterSort = {
    id:'asc',
    status:'asc',
    address_ip: 'asc',
    name:'asc'
  }

  constructor(private dataManagerService:DataManagerService,private modalService: NgbModal) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop',
    };

  }

 statusNameString(id:number){
    for(let s of PRINTER_STATUS){
      if(s.id == id){
        return s.name;
      }
    }
  }

  ngOnInit(){
    this.uploadData();
  }

  uploadData(){
    this.dataManagerService.getStorage().subscribe(res=>{
      this.copyPrinters = cloneDeep(res.printers);
      this.dataStorage = cloneDeep(res);
      this.loaderStatus = false;
    })
  }


  editPrinterForm(data: any,content:any){

  }

  closePrinterForm(data:any){

  }


  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Sort printers array
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  sortPrinterArr(param: string,e:any){

    for(let t in this.tablePrinterSort){
      this.tablePrinterSort[t] = 'asc';
    }

    var activeOrderBy = Number(e.srcElement.dataset.sort);
    var tempArr = [];

    if(activeOrderBy == 1){
      this.copyPrinters.sort(this.getSortOrder(param,2));
      e.srcElement.dataset.sort = 2;
      this.tablePrinterSort[param] = 'desc';
    }else{
      this.copyPrinters.sort(this.getSortOrder(param,1));
      e.srcElement.dataset.sort = 1;
      this.tablePrinterSort[param] = 'asc';
    }

  }

  getSortOrder(prop:string, direction:number) {
    if(direction == 1) //asc
    {
      return function(a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
       }

    }else if(direction == 2){ //desc
      return function(a, b) {
        if (a[prop] < b[prop]) {
            return 1;
        } else if (a[prop] > b[prop]) {
            return -1;
        }
        return 0;
       }
    }

}

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Color row of printers table
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  printersListColorActiveRow(row_index:number){
    $(".printer-row-*").css({"background":"white"})
    $(".printer-row-"+row_index).css({"background":"#ececec"})
  }

  printersListNoneColor(){
    $(".printer-row").css({"background":"white"})
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Remove printer
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  removePrinterForm(data:any, content:any,row_index:number){

    this.printersListColorActiveRow(row_index);

        this.modalWindow.title = 'Komunikat';
        this.modalWindow.message = "Czy na pewno chcesz usunąć urządzenie <b>"+data.name+"</b> ?";
        this.modalWindow.status = false;
        this.modalWindow.type = 'remove';
        this.modalWindow.activePrinter = data;
        this.modalWindow.rightButtonContent = "Anuluj";


    this.modalService.open(content, this.modalOptions).result.then(
      result => {},
      reason => {}
    );
  }

  removePrinter(content:any){
    if(this.modalWindow.activePrinter!=null && this.dataStorage.printers.length>0){

      this.dataManagerService.removePrinter(this.modalWindow.activePrinter.id);
      this.removePrinterSuccess();
      this.uploadData()
    }
  }

  removePrinterSuccess(){
    this.modalWindow.message = "Urządzenie zostało usunięte.";
    this.modalWindow.rightButtonContent = "Zamknij";
    this.modalWindow.status = true;
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Display printer info
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  displayPrinterInfo(data:any, content:any,row_index:number){

  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Modal window
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  closeModal(){
    this.printersListNoneColor();
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Refresh printers table
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  refreshPrintersTable(){
    this.searchText = '';
    this.uploadData();

  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Search printer
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  searchPrinter(){
    this.searchText = this.searchText.toLowerCase();
    if(this.searchText.length>0){

      var arr = [];

      for(let it of this.copyPrinters){
        var count = 0;
        if(this.searchText.length>0){

          if(it.id.toString().includes(this.searchText)){
            count++;
          }
          if(it.name.toLowerCase().includes(this.searchText)){
            count++;
          }
          if(it.address_ip.includes(this.searchText)){
            count++;
          }
          if(it.location.toLowerCase().includes(this.searchText)){
            count++;
          }

          if(count>0){
            arr.push(it);
          }
        }else{
          arr.push(it);
        }

      }
      this.copyPrinters = cloneDeep(arr)

    }else{
      this.copyPrinters = this.dataStorage.printers;
    }
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Generate pdf report
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  generatePdfReport(data:any, content:any){

    var genData = cloneDeep(data);
    let name_temp = (data.name).replace(' ','_');

    genData.file_name ='Raport_'+ name_temp;

    genData.useColor = this.getUseColorName(data.useColor);
    this.loaderStatus = true;

    // console.log(genData,data)

    this.dataManagerService.generatePrinterReportPdf(genData).subscribe(
      res=>{
        this.loaderStatus = false;
        const url = environment.api_dir_pdf + res;
        let a_elem = document.createElement('a');
        a_elem.setAttribute('href', '' + url);
        a_elem.setAttribute('download', genData.file_name + '.pdf');
        a_elem.setAttribute('target', '_blank');
        document.body.append(a_elem);
        a_elem.click();
        a_elem.remove();
      },
      err=>{
        console.log(err)
      }
    )

  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Get name of data
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  getUseColorName(id:number){
    console.log(id,PRINTER_COLORS)
    for(let s of PRINTER_COLORS){
      if(s.id == id){
        console.log(s.name)
        return s.name;
      }
    }
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Generate txt file - not use
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  generateTextFile(data:Printer){
    let textFile = null;
    const makeTextFile = ((text: string)=> {
          const dataFile = new Blob([text], {type: 'text/plain'});

          if (textFile !== null) {
            window.URL.revokeObjectURL(textFile);
          }

          textFile = window.URL.createObjectURL(dataFile);
          return textFile;
    });

    let textContent='';
    textContent += "Nazwa urządenia: "+data.name+"\r\n";
    textContent += "Status urządzenia: "+data.status+"\r\n";
    textContent += "Obsługa kolorów: "+data.useColor+"\r\n";
    textContent += "Adres IP: "+data.address_ip+"\r\n";
    textContent += "Lokalizacja: "+data.location+"\r\n";
    textContent += "Opis: "+data.desc+"\r\n";

      let a_elem = document.createElement('a');
      a_elem.setAttribute('href', makeTextFile(textContent));
      a_elem.setAttribute('download', 'Report.txt');
      document.body.append(a_elem);
      a_elem.click();
      a_elem.remove();
  }
}


