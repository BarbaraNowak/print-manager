import { cloneDeep } from 'lodash';
import { DataManagerService } from './../../../../global-services/data-manager.service';
import { Printer } from './../../../../global-data/printers';
import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { Data } from 'src/app/global-data/data';


@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.scss']
})
export class MainPanelComponent implements OnInit {

  dataStorage: Data | any;
  loaderStatus: boolean = false;

  modalOptions: NgbModalOptions;
  modalWindow = {
    type: '',
    title: '',
    message: '',
    status: false,
    activePrinter:null,
    rightButtonContent: ''
  };

  closeForm = 0;

  constructor(private dataManagerService: DataManagerService, private modalService: NgbModal) {
   }

  ngOnInit(): void {
    this.loaderStatus = true;
    this.uploadData();
  }

  uploadData(){

      this.dataManagerService.getStorage().subscribe(res=>{
        this.dataStorage = cloneDeep(res);
        this.loaderStatus = false;
      })
  }


  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Add printer to dataset
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  addPrinter(printer:Printer){

    this.dataStorage.printers.push(printer);
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Close modal window - add printer
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  closePrinterForm(data:any){
    this.closeForm = data;
    this.modalWindow.message = "Urządzenie zostało poprawnie dodane."

  }

}
