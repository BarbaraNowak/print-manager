import { TooltipBasic } from './layout/tooltip/tooltip-basic';
import { ErrorComponent } from './error/error.component';
import { ChartsAreaComponent } from './chart/chart-types/charts-area/charts-area.component';
import { LoaderComponent } from './layout/loader/loader.component';
import { PrinterViewComponent } from './printer-view/printer-view.component';
import { LayoutPanelComponent } from './layout/layout-panel/layout-panel.component';
import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component';
import { PanelContentComponent } from './layout/panel-content/panel-content.component';
import { MenuLeftComponent } from './layout/menu-left/menu-left.component';
import { SvgIconComponent } from './svg-icon/svg-icon.component';
import { PrinterFormComponent } from './printer-form/printer-form.component';
import { PrintersListComponent } from './printers-list/printers-list.component';
import { MainPanelComponent } from './main-panel/main-panel.component';

export const containers = [MainPanelComponent, PrintersListComponent, PrinterFormComponent,
   SvgIconComponent, MenuLeftComponent, PanelContentComponent, BreadcrumbComponent,LayoutPanelComponent, PrinterViewComponent,
   LoaderComponent, ChartsAreaComponent, ErrorComponent, TooltipBasic

  ];

export * from './main-panel/main-panel.component';
export * from './printers-list/printers-list.component';
export * from './printer-form/printer-form.component';
export * from './svg-icon/svg-icon.component';
export * from './layout/menu-left/menu-left.component';
export * from './layout/panel-content/panel-content.component';
export * from './layout/breadcrumb/breadcrumb.component';
export * from './layout/layout-panel/layout-panel.component';
export * from './printer-view/printer-view.component';
export * from './layout/loader/loader.component';
export * from './chart/chart-types/charts-area/charts-area.component';
export * from './error/error.component';
export * from './layout/tooltip/tooltip-basic';
