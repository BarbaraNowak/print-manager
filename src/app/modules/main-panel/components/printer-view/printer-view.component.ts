import { PRINTER_STATUS } from './../../../../global-data/printer-status';
import { cloneDeep } from 'lodash';
import { DataManagerService } from 'src/app/global-services/data-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/global-services/breadcrumb.service';
import { Printer } from 'src/app/global-data/printers';
import { PRINTER_COLORS } from 'src/app/global-data/printer-colors';
import { Data } from 'src/app/global-data/data';


@Component({
  selector: 'app-printer-view',
  templateUrl: './printer-view.component.html',
  styleUrls: ['./printer-view.component.scss']
})
export class PrinterViewComponent implements OnInit {

  breadcrumbs: [];
  checkUrl: string;
  activePrinter: Printer;
  activePrinterId: number;
  activePrinterIndex: number;
  dataStorage: Data | any = {
    printers:[]
  };

  printerStatus: string;
  printerColor: string;

  chartLabels = [];
  chartData = [];

  constructor(private bcService: BreadcrumbService, private router: Router, private route: ActivatedRoute,
    private dataManagerService: DataManagerService) { }

  ngOnInit(): void {
    this.uploadData();
  }

  uploadData(){

      this.dataManagerService.getStorage().subscribe(res=>{
        this.dataStorage = cloneDeep(res);

        if(this.route.snapshot.url.length>0){
          this.activePrinterId = Number(this.route.snapshot.url[1].path);
        }

        if(this.dataStorage.printers.length==0){
          this.router.navigate(['/print-panel']);
        }

        this.setBreadcrumbs();
        this.getActivePrinterData();
        this.getPrinterStatusName();
        this.getPrinterColor();
        this.setChartData()
      })
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Set chart data - values and labels
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  setChartData(){
    for(let h of this.dataStorage.printersHistory){
      if(h.printer_id == this.activePrinterId){
        this.chartLabels.push(h.time)
        this.chartData.push(h.status)
      }
    }

  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Set component breadcrumbs
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  setBreadcrumbs(){
    this.bcService.getBreadcrumbs().subscribe(dataBc => {

      this.checkUrl = this.route.snapshot.url[0].path;

      switch(this.checkUrl){
        case 'view':
          this.breadcrumbs = dataBc.printer.view;
        break;
      }
    });
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Get data of active printer
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  getActivePrinterData(){
    var i=0;
    for(let p of this.dataStorage.printers){
      if(p.id == this.activePrinterId){
        this.activePrinter = cloneDeep(p);
        this.activePrinterIndex = i;
      }
      i++;
    }
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Get printer status name
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  getPrinterStatusName(){
    for(let s of PRINTER_STATUS){
      if(s.id == this.activePrinter.status){
        this.printerStatus = s.name;
      }
    }
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Get printer color
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  getPrinterColor(){
    for(let c of PRINTER_COLORS){
      if(c.id == this.activePrinter.useColor){
        this.printerColor = c.name;
      }
    }
  }

}
