import { PRINTER_COLORS } from './../../../../global-data/printer-colors';
import { PRINTER_STATUS } from './../../../../global-data/printer-status';
import { cloneDeep } from 'lodash';

import { Printer } from './../../../../global-data/printers';
import { Component, OnInit, Output, EventEmitter, Input, ɵConsole } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataManagerService } from 'src/app/global-services/data-manager.service';
import { RxwebValidators,IpVersion } from '@rxweb/reactive-form-validators';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from 'src/app/global-services/breadcrumb.service';
import { Data } from 'src/app/global-data/data';



@Component({
  selector: 'app-printer-form',
  templateUrl: './printer-form.component.html',
  styleUrls: ['./printer-form.component.scss']
})
export class PrinterFormComponent implements OnInit {

  dataStorage: Data | any;
  date = new Date();
  printerStatusArr = PRINTER_STATUS;
  printerColorArr = PRINTER_COLORS;
  breadcrumbs: [];

  // @Output() closeForm = new EventEmitter();
  @Input() formType : string;
  activePrinter: Printer;
  activePrinterId: number;
  activePrinterIndex: number;

  newPrinterObject: Printer;
  updatePrinterObject: any;
  printerForm: any;
  checkUrl: any;

  submitStatus = false;
  buttonContent : string;


  constructor(private dataManagerService:DataManagerService, private router: Router,private route: ActivatedRoute,
    private bcService: BreadcrumbService) {


    this.printerForm = new FormGroup({
      printerName: new FormControl('', Validators.required),
      printerAddress: new FormControl('', [Validators.required, RxwebValidators.ip({version:IpVersion.AnyOne})]),
      printerStatus: new FormControl(0),
      printerDesc: new FormControl(''),
      printerLocation: new FormControl('', Validators.required),
      printerColor: new FormControl(0),
      printerFormat: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.uploadData();
  }


  uploadData(){
    this.dataManagerService.getStorage().subscribe(res=>{
      this.dataStorage = cloneDeep(res);
      this.checkUrl = this.route.snapshot.url[0].path;

      this.fillPrinterForm();
      this.setBreadcrumbs();
    })
}

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Set component breadcrumbs
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  setBreadcrumbs(){
    this.bcService.getBreadcrumbs().subscribe(dataBc => {
      switch(this.checkUrl){
        case 'create':
          this.breadcrumbs = dataBc.printer.create;
        break;
        case 'edit':
          this.breadcrumbs = dataBc.printer.edit;
        break;
      }

  });
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Get data of active printer
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  getActivePrinterData(){
    var i=0;
    for(let p of this.dataStorage.printers){
      if(p.id == this.activePrinterId){
        this.activePrinter = cloneDeep(p);
        this.activePrinterIndex = i;
      }
      i++
    }
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Fill printer form  - create / edit
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  fillPrinterForm(){
    console.log('fill-'+this.checkUrl)
    if(this.dataStorage.printers.length==0){
      this.router.navigate(['/print-panel']);
    }

    switch(this.checkUrl){

      case 'create':
        this.buttonContent = 'Dodaj urządzenie';
      break;
      case 'edit':
        this.buttonContent = 'Zapisz zmiany';

        if(this.route.snapshot.url.length>1){

          this.activePrinterId = Number(this.route.snapshot.url[1].path);

          this.getActivePrinterData();

          if(this.activePrinter){

            this.printerForm = new FormGroup({
              printerName: new FormControl(this.activePrinter.name, Validators.required),
              printerAddress: new FormControl(this.activePrinter.address_ip, [Validators.required, RxwebValidators.ip({version:IpVersion.AnyOne})]),
              // printerStatus: new FormControl(),
              printerDesc: new FormControl(this.activePrinter.desc),
              printerLocation: new FormControl(this.activePrinter.location, Validators.required),
              printerStatus: new FormControl(this.activePrinter.status),
              printerColor: new FormControl(this.activePrinter.useColor),
              printerFormat: new FormControl(this.activePrinter.printFormat)
            });
          }

        }

      break;

    }
  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Printer form - submit
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  onSubmit(){
    this.submitStatus = true;
    var formStatus = this.printerForm.status;

    if(this.submitStatus && formStatus == 'VALID'){

      switch(this.checkUrl){

        case 'create':
            this.newPrinterObject = {
              id:this.getMaxPrintersId() + 1,
              name: this.checkSpecialChars(this.printerName.value),
              status: 1,
              desc: this.printerDesc.value,
              address_ip: this.printerAddress.value,
              location: this.printerLocation.value,
              printFormat: this.printerFormat.value,
              useColor: this.printerColor.value
            } as Printer;

            this.dataManagerService.addPrinter(this.newPrinterObject);

        break;
        case 'edit':

          this.updatePrinterObject = {
            name: this.printerName.value,
            desc: this.printerDesc.value,
            address_ip: this.printerAddress.value,
            location: this.printerLocation.value,
            useColor: this.printerColor.value,
            status: this.activePrinter.status,
            printFormat: this.printerFormat.value
          }
          this.dataManagerService.updatePrinter(this.activePrinterId,this.updatePrinterObject)

        break;
      }

        this.router.navigate(['/print-panel']);


    }else{
      // different error

    }
  }

  checkSpecialChars(name:string){
    let name_temp = name.replace(/[&\/\\#,+()$~%.'":*?<>^{}]/g, '');
    return name_temp;
  }

  clickCloseForm(){

  }

  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Get form values as variable
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  get printerName(){ return this.printerForm.get('printerName')}
  get printerAddress(){ return this.printerForm.get('printerAddress')}
  get printerLocation(){ return this.printerForm.get('printerLocation')}
  get printerDesc(){ return this.printerForm.get('printerDesc')}
  get printerColor(){ return this.printerForm.get('printerColor')}
  get printerFormat(){ return this.printerForm.get('printerFormat')}


  /* * * * * * * * * * *  * * * * * * * * * * * ** * * * * * * * * * * * * * *
  # Get max printer id
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  getMaxPrintersId(){
    var max_id=0;
    Math.max.apply(Math,this.dataStorage.printers.map(function(o){
      max_id = o.id;
    }))
    return max_id;
  }


}
