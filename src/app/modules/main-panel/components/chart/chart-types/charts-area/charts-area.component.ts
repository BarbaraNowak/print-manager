import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    OnInit,
    ViewChild,
    Input,
} from '@angular/core';
import { Chart } from 'chart.js';
import { PRINTER_STATUS } from 'src/app/global-data/printer-status';

@Component({
    selector: 'app-chart-area',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './charts-area.component.html',
    styleUrls: ['charts-area.component.scss'],
})
export class ChartsAreaComponent implements OnInit, AfterViewInit {
    @ViewChild('myAreaChart') myAreaChart!: ElementRef<HTMLCanvasElement>;
    chart!: Chart;

    @Input() chartLabels:[];
    @Input() chartData:[];
    @Input() chartYmin:number;
    @Input() chartYmax:number;

    constructor() {}
    ngOnInit() {}

    ngAfterViewInit() {

        this.chart = new Chart(this.myAreaChart.nativeElement, {
            type: 'line',
            data: {
                labels: this.chartLabels,
                datasets: [
                    {
                        label: 'Status',
                        // label: ['Wyłączone','Włączone'],
                        lineTension: 0.3,
                        backgroundColor: 'rgba(2,117,216,0.2)',
                        borderColor: 'rgba(2,117,216,1)',
                        pointRadius: 5,
                        pointBackgroundColor: 'rgba(2,117,216,1)',
                        pointBorderColor: 'rgba(255,255,255,0.8)',
                        pointHoverRadius: 7,
                        pointHoverBackgroundColor: 'rgba(2,117,216,1)',
                        pointHitRadius: 20,
                        pointBorderWidth: 2,
                        data: this.chartData

                    },
                ],
            },
            options: {
                scales: {
                    xAxes: [
                        {
                            time: {
                                unit: 'time',
                            },
                            gridLines: {
                                display: false,
                            },
                            ticks: {
                                maxTicksLimit: 7,
                            },
                        },
                    ],
                    yAxes: [
                        {
                            ticks: {
                                min: this.chartYmin,
                                max: this.chartYmax,
                                maxTicksLimit: 5,
                                callback: function(value, index, values) {
                                  for(let s of PRINTER_STATUS){
                                    if(s.id == value){
                                      return s.name;
                                    }
                                  }

                              }
                            },
                            gridLines: {
                                color: 'rgba(0, 0, 0, .125)',
                            },
                        },
                    ],
                },
                legend: {
                    display: false,
                },
                tooltips: {
                  callbacks: {
                      label: function(tooltipItem, data) {
                          var label = data.datasets[tooltipItem.datasetIndex].label || '';

                          if (label) {
                              label += ': ';
                          }
                          for(let s of PRINTER_STATUS){
                            if(s.id == tooltipItem.yLabel){
                              label +=  s.name;
                            }
                          }

                          return label;
                      }
                  }
              }
            },
        });
    }
}
