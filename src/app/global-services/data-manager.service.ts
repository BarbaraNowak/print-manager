import { Printer } from './../global-data/printers';
import { Data } from './../global-data/data';
import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import * as PRINTERS from './../../assets/data/printers.json';
import { cloneDeep } from 'lodash';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';

import * as PRINTERS_HISTORY from './../../assets/data/printers_history.json';
import { environment } from 'src/environments/environment';
import { PRINTER_COLORS } from '../global-data/printer-colors';

@Injectable({
  providedIn: 'root'
})
export class DataManagerService{
  // printersArr: [any] = (PRINTERS as any).default;
  apiUrl = 'assets/data/printers.json';
  jsonDataPrinters:any = PRINTERS;
  jsonDataPrintersHistory:any = PRINTERS_HISTORY;

  dataStorage: Data | any;
  printers: Printer[];

  environmentData = environment;

  constructor(private http: HttpClient) {
    this.dataStorage =  {
      printers: this.jsonDataPrinters.default.printers,
      printersHistory: this.jsonDataPrintersHistory.default.printers_history
    }
    console.log(this.dataStorage.printers)
  }

  getStorage(): Observable<any> {
    return of(this.dataStorage);
  }

  setStorage(data: Data){
    this.dataStorage = data;
  }

  setPrinters(value:Printer[]){
    this.printers = value;
  }

  addPrinter(data:Printer){
    this.dataStorage.printers.push(data);
  }

  updatePrinter(id:number,data:any){
    for(let p of this.dataStorage.printers){
      if(p.id == id){
        p.name = data.name;
        p.status = data.status;
        p.address_ip = data.address_ip;
        p.location = data.location;
        p.desc = data.desc;
        p.useColor = data.useColor;
        p.printFormat = data.printFormat;
      }
    }
  }

  removePrinter(id:number){
    this.dataStorage.printers = this.dataStorage.printers.filter(d => d.id !== id);
  }

  generatePrinterReportPdf(data:any){
    return this.http.post(`${this.environmentData.api_dir_pdf}/createPrinterReportPdf.php`,
    // return this.http.post('./../global-data/pdf/createPrinterReportPdf.php',
      data,
      {
          headers: new HttpHeaders({
              'Content-Type': 'text',
              Accept: 'application/pdf',
              'Response-Type': 'arraybuffer', // blob
          }),
      }
  );
  }

}
