import { DataManagerService } from 'src/app/global-services/data-manager.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class BreadcrumbService {
    breadcrumbs: any;
    dataStorage: any;
    constructor(private dataService: DataManagerService) {
        this.dataService.getStorage().subscribe(datastg => {
            this.dataStorage = datastg;
        });

        this.breadcrumbs = {

            printer:{
              create: [
                {
                    text: 'Drukarki',
                },
                {
                    text: 'Kreator urządzenia',
                    active: true,
                },
              ],
              edit: [
                {
                    text: 'Drukarki',
                },
                {
                    text: 'Edytor urządzenia',
                    active: true,

                },
             ],
             view: [
              {
                  text: 'Drukarki',
              },
              {
                  text: 'Zestawienie danych dla urządzenia ',
                  active: true,

              },
           ],
             list:[
              {
                text: 'Drukarki',
              },
              {
                text: 'Lista urządzeń',
                active: true,
              },
             ]
            }


        };
    }

    ngOnInit() {}

    getBreadcrumbs(): Observable<any> {
        return of(this.breadcrumbs);
    }
}
