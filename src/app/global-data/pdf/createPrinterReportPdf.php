<?php
// require '../database.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Response-Type");


require __DIR__.'./vendor/autoload.php';
include './snappyPrinterReportPdf.php';
$postdata = file_get_contents("php://input");


if(isset($postdata) && !empty($postdata))
{
  $request = json_decode($postdata);

  $requestArr = [];

  $file_name = $requestArr[0]['file_name'] = trim($request->file_name);
  $name = $requestArr[1]['name'] = trim($request->name);
  $status = $requestArr[2]['status'] = trim($request->status);
  $useColor = $requestArr[3]['useColor'] = trim($request->useColor);
  $location =  $requestArr[4]['location'] = trim($request->location);
  $desc = $requestArr[5]['desc'] = trim($request->desc);
  $address_ip = $requestArr[6]['address_ip'] = trim($request->address_ip);
  $printFormat = $requestArr[7]['printFormat'] = trim($request->printFormat);

//   $getarr = array();
//   $i=0;
//   foreach($request->receive as $k=>$v) {
//       $getarr[$k] = mysqli_real_escape_string($con, $v);
//       $i++;

//   }
//   $receive=$getarr;


  $snappyPdf = new SnappyPdf();
  $outputFilePath = $snappyPdf->makePdf($file_name,$requestArr);

  echo json_encode($outputFilePath);
}
?>

