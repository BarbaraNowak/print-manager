
<?php
use Knp\Snappy\Pdf;


class SnappyPdf{

    public function __construct()
    {

    }

    public function makePdf($name,$requestArr)
    {
        $name = implode('',$requestArr[1]);
        // $status = implode('',$requestArr[2]);
        $useColor = implode('',$requestArr[3]);
        $location = implode('',$requestArr[4]);
        $desc = implode('',$requestArr[5]);
        $address_ip= implode('',$requestArr[6]);
        $printFormat= implode('',$requestArr[7]);


        $css_File = file_get_contents('../../../../node_modules/bootstrap/dist/css/bootstrap.min.css');

        $snappy = new Pdf(realpath('./vendor/wemersonjanuario/wkhtmltopdf-windows/bin/64bit/wkhtmltopdf.exe'));

        //linux
        //$snappy = new Pdf('./vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');

        //windows
        $snappy = new Pdf(realpath(__DIR__.'/vendor/wemersonjanuario/wkhtmltopdf-windows/bin/64bit/wkhtmltopdf.exe'));

        header('Content-Type: application/pdf');
        header("Content-Disposition: attachment; filename=$name.pdf");

        $snappy->setOption('margin-top', 10);
        $snappy->setOption('margin-bottom', 20);
        $snappy->setOption('margin-left', 10);
        $snappy->setOption('margin-right', 10);
        // $snappy->setOption('footer-center', 'Page [page] of [toPage]');
        $snappy->setOption('footer-right', '[page]/[toPage]');
        $snappy->setOption('encoding', 'UTF-8');
        $snappy->setOption('user-style-sheet','./styles/style-printer-report.scss');


        //usuniecie pliku

        /*
        $plik = __DIR__.'/files/printer-reports/'.$name.'.pdf';
        $test = file_exists($plik);
        if($test)
        {
            unlink($plik);
        }
        */

        $snappy->generateFromHtml(str_replace(
            array(
                '%name%',
                '%useColor%',
                '%location%',
                '%desc%',
                '%address_ip%',
                '%printFormat%'
            ),
            array(
                $name,
                $useColor,
                $location,
                $desc,
                $address_ip,
                $printFormat


            ),
            file_get_contents("./html/printer-report-pdf.html")
        ),'./files/printer-reports/'.$name.'.pdf',Array(),true);

        return '/files/printer-reports/'.$name.'.pdf';
    }
}
?>
