export interface Printer{
  id: number;
  name: string;
  status: number; // 0-offline | 1-online | 2-warning | 3-maintenance
  desc: string;
  address_ip: string;
  location: string;
  useColor: number; // 0-no | 1-yes
  printFormat: string;
}
