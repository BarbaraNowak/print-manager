import { MainPanelModule } from './../../modules/main-panel/main-panel.module';
/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


/* Containers */
import * as mainPanelContainers from './../../modules/main-panel/components';

import { RouteData } from './breadcrumb-data';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        canActivate: [],
        component: mainPanelContainers.MainPanelComponent,
        data: {
            title: 'Tables - SB Admin Angular',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/dashboard',
                },
                {
                    text: 'Tables',
                    active: true,
                },
            ],
        } as RouteData,
    },
];

@NgModule({
    imports: [MainPanelModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class BreadcrumbsRoutingModule {}
