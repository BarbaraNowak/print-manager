
export interface BreadcrumbData{
  link?: string;
  active?: boolean;
  text: string;
}

export interface SideNavItems {
  [index: string]: SideNavItem;
}

export interface RouteData {
  title?: string;
  activeTopNav?: string;
  breadcrumbs: BreadcrumbData[];
}

export interface SideNavItem {
  icon?: string;
  text: string;
  link?: string;
  submenu?: SideNavItem[];
  userEnable?: boolean;
  adminEnable?: boolean;
}

export interface SideNavSection {
  text?: string;
  items: string[];
  userEnable?: boolean;
  adminEnable?: boolean;
}
