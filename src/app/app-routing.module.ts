import { ErrorComponent } from './modules/main-panel/components/error/error.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () =>
        import('./modules/start-page/start-page-routing.module').then(
            m => m.StartPageRoutingModule
        ),
    canActivate: [],
  },
  {
    path: 'print-panel',
    loadChildren: () =>
        import('./modules/main-panel/main-panel-routing.module').then(
            m => m.MainPanelRoutingModule
        ),
    canActivate: [],
  },
  {
    path: '**',
    pathMatch: 'full',
    component: ErrorComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
