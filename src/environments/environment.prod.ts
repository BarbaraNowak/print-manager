
// const APP_URL = location.protocol + '//domena.../print-manager';
const APP_URL = 'http://127.0.0.1/angular/mc-comp/print-manager'; // home

export const environment = {
  production: true,
  api_dir_pdf: `${APP_URL}/src/app/global-data/pdf`,
};
